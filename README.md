![Build Status](https://gitlab.com/pages/plain-html/badges/master/build.svg)

---

Card art generator for stuffy (WIP)

---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [GitLab CI](#gitlab-ci)
- [GitLab User or Group Pages](#gitlab-user-or-group-pages)
- [Did you fork this project?](#did-you-fork-this-project)
- [Troubleshooting](#troubleshooting)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Card dimensions
Cards are set to 2.5 in x 3.5 in (W x H). The site is currently set to 150dpi, ending with a resolution of 375 x 525 pixels.

More details to follow.
