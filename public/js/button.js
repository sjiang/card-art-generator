$(function() { 
    $("#btnConvert").click(function() { 
        html2canvas($("#widget"), {
            onrendered: function(canvas) {
                theCanvas = canvas;
                document.body.appendChild(canvas);
                $("#img-out").empty();
                // Convert and download as image
                $("#img-out").append(Canvas2Image.convertToImage(canvas));
                // Clean up 
                document.body.removeChild(canvas);
            }
        });
    });

    $("#btnSave").click(function() { 
        html2canvas($("#widget"), {
            onrendered: function(canvas) {
                theCanvas = canvas;
                document.body.appendChild(canvas);
                $("#img-out").empty();
                // Convert and download as image
                Canvas2Image.saveAsImage(canvas);
                // Clean up 
                document.body.removeChild(canvas);
            }
        });
    });

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('#picture').attr('src', e.target.result);
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#imgImp").change(function(){
        readURL(this);
    });

    $(".statsCheck").click(function() {
        if($(this).is(":checked")) {
            $(".stats").show();
        } else {
            $(".stats").hide();
        }
    });

    $('.marginEdit').on('change', function(){
        $('.left-margin').text($(this).val() + 'px');
        $('.image').css({'margin-left': $(this).val()});
    });

    $('.fontSizeEdit').on('change', function(){
        $('.font-size').text($(this).val() + 'em');
        $('.content').css({'font-size': $(this).val() + 'em'});
        $('.content').css({'line-height': $(this).val()*24 + 'px'});
    });

    $(".statsCheck").prop("checked",true);
    $(".marginEdit").val(0);
    $("#imgImp").val("");
    $(".fontSizeEdit").val(1);
}); 


