General theme of this deck is real lighthearted and cartoony cheerful candies. Try to really play up how 'childrens cartoon' style they are, but they've got a ~dark secret~ 

Cherry on top
A real cheery, bright red shiny cherry, with a top hat. He can be dressed up in a suit or not, but the hat is 100% mandatory. Bonus points if he looks kind of snobby.

Ice cream daddy
A roughly personshaped blob of icecream, maybe in a skin tone, but it could work with more regular icecream colours also. Make him look like a sex offender, with a big ol' 80s porno stache made of wafers, some sunglasses, and probably a balding head. He should have chocolate sprinkles on his 'chest' that look like chest hair. Either give him a really skimpy bowl where the pants would be on a person, or just cut off the picture under his hairy chest.

Unthinkably Many Tiered Cakestravaganza 
A gelatinous, beastly cake with many layers stacked on top of each other. Give it cartoony fat googly eyes, and make it look like its crushing itself under its own mass. I want to kind of evoke a cthulhuesque unthinkable feel with this, probably have it fill as much of the space as possible. I'm imagining some nice colourful fruits, possibly some little wedding figurines washing around in its girthy mass, but that might sound better than it looks.

Ceaseless candies
I'm imagining 2-3 pieces of the same kind of candy, pulling themselves out of a gigantic mass of the same sweet material with just one of them being mostly fully formed, in the same vein of this one scene from lord of the rings
https://www.youtube.com/watch?v=6jamrudGfC4 around thirty seconds. Try to make the mostly finished one look real cute and cartoony, but the other ones still partially stuck in can look a little more incomplete and grotesque. This can be just about any kind of candy, I kind of had the idea of candy corn, but really any sort of mushy candy will do.

Curse of the Chronic Donuts
A donut in a convertable doing donuts in a parking lot. Make it look like he's kind of tired and droopy. He's been doing these donuts for a really long time, but he just can't stop. If its possible to make him look sweaty and tired, that'd be great, but I'm not sure how you can make a donut look like that.

Atrocious Chocolate Transfusion
A desiccated chocolate bar is partially unwrapped, with a IV looking tube pulling chocolate out of it, while a much healthier, youthful looking chocolate bar with a fiendish looking mustache is either flexing or posing nearby.

Grapes of wrath 
Any sort of candy man eating a handful of small grapes. He should have a bit of a jekyll/hyde dynamic going on, with half of him looking monsterous and muscular and angry, and the other half of him looking regular and cute. Grapejuice should be spiling down his face, and he should have a confused-about-why-im-half-monster type face.

Smore Smore
A tiny little gingerbread house with a giant marshmallow-man oozing out from all the windows and doors. The marshmallow should have a manaical look on his face as he grows larger.